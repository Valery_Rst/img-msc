package net.rstvvoli.ImageAndMusic;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;

public class Main {
    private static final String SRC = "src\\source.txt";
    private static String sourceToImageLink;
    private static String sourceToMusicLink;
    private static String pathToImage;
    private static String pathToMusic;

    public static void main(String[] args) throws MalformedURLException {

        readFileAndExtract();

        DownloadThread image = new DownloadThread(sourceToImageLink, pathToImage);
        DownloadThread music = new DownloadThread(sourceToMusicLink, pathToMusic);

        image.start();
        music.start();

        try {
            /* Метод join() вызвпн для того, чтобы
             * подождать пока завершиться поток скачивания музыки,
             * и только после этого проиграть её - так логичнее;
             */
            music.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        play(pathToMusic);
    }
    /**
     * Чтение файла, в котором расположенны ссылки
     * для скачивания музыки и изображения,
     * и извлечение полученных ссылок в поля класса;
     */
    private static void readFileAndExtract() {
        try(BufferedReader inFile = new BufferedReader(new FileReader(SRC))) {

            String[] link = inFile.readLine().split(" ", 2);
            sourceToImageLink = link[0];
            pathToImage = link[1];

            link = inFile.readLine().split(" ", 2);
            sourceToMusicLink = link[0];
            pathToMusic = link[1];
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * Метод проигрывает скачанную ранее музыку по пути к музыке;
     */
    private static void play(String path) {
        try (FileInputStream inputStream = new FileInputStream(path)) {
            try {
                Player player = new Player(inputStream);
                player.play();
            } catch (JavaLayerException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}